

var base_url =
  "https://api.spotify.com/v1/search?query={artist_name}&type=artist&market=US&offset=0&limit=20";


//token
var aToken = "BQCLAQ0AHPSuH9F9h4tyZHRhumCv7fT9aRa4BlkiXaVXQjeUxYd5uKan-DLA3OkeULMayOx3XPMxxfHFB2ZqGP33a7DvIYA_j9BX0AYFJZ_aSbbuhxio8zry7jkLuhEi9gEdQdCg9-SBxf3_1Auo_IhDkrNTfYv2Y8gH5ikPWRQpiC1eWX5wliWWjc64gzZ5mla9vmU5LBfp3vleI1AI0qp_b7iOFLus-kUYOuktM-DvdiFTImu8laTuk2JiOxU7UXcXkDGk9HgM0f1itrwQIZ9TnqclEg";

//artist name to url
function getSearchUrl(name) {
  var url = base_url;
  var artistName = $("#artist-name").val();
  url = url.replace(/{artist_name}/, artistName);
  return url;
}

function api_search(artist_name, oAuth_token) {
  let oAuthToken = oAuth_token;
  let new_url = getSearchUrl(artist_name);
  console.log("search URL: " + new_url);
  
  
  if (!new_url || new_url.indexOf("{artist_name}") > -1) {
    console.log("Wrong url: " + new_url);
    return false;
  }
  $("#search-url").val(new_url);
    
  // call ajax
  $.ajax({
    method: "GET",
    url: new_url,
    dataType: "json",
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      Authorization: "Bearer " + oAuthToken
    },
    success: function(data) {
     
      
     //convert data
      var myJSON = JSON.stringify(data);
      $("textarea#spotify-data").text(myJSON);

     //follower
      let followers = data.artists.items[0].followers.total;
      console.log("Followers:" + followers);
      $("span#followers").text(followers);
      //artist name
      let artistName = data.artists.items[0].name;
      $("#out_name").text(artistName);
      //artist id
      let artistID = data.artists.items[0].id;
      $("#artist_id").val(artistID);
      //genres
      let genres = data.artists.items[0].genres;
      $("#genre").text(genres);
      //artist url
      let url = data.artists.items[0].href;
      $("#artist_url").text(url);
    //artis img
      let img_src = data.artists.items[0].images[2].url;
      $("#artist_image").attr("src", img_src);
    },
    error: function(e) {
      let err = JSON.stringify(e);
      console.log(err);
    },
    cache: false
  });
}




  $("#send-req").click(function() {
    console.log("Send Button Hit!");
    let artist_name = $("#artist-name").val();
    console.log("Artist ID: " + artist_name);

   
    let oAuth_token = aToken;
    if ($("input#auth-token").val() !== "") {
      oAauth_token = $("input#auth-token").val();
    }
    
   
    console.log("OAuth Token: " + oAuth_token);
    $("input#auth-token").val(oAuth_token);
    api_search(artist_name, oAuth_token);
  });
};